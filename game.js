var lastTime = Date.now(), timeSinceLastFrame = 0;


//characters sprites
var A, B, C, D, E;

//animations
var animA, animB, animC, animD, animE;
var bg, title;
var xButton;

//sounds
var music, hit;

//boolean
var musicIsPlaying = true;

var POS_Y = 392;

var TEXT_Y = 150;
var TEXT_W = 380;
var TEXT_H = 800;

var age;
var phase;
var phaseCounter;
var gameState;

//timing in frames (at 60FPS)
var ONE_DELAY = 35;
var FADE_DELAY = 30;
var MSG_DELAY = 75;

//textFields
var oneOfThem;
var prompt;
var response;

//font
var fontNormal;
var fontBold;

var instructions;

var words = [
[
    "L'une d'elles... ",
    "...s'appelle Sandra ",
    "Ga?",
    "Pfflltltl",
    "San...dra...",
    "Hihi!",
    "ADRA!"
],
[
    "L'une d'elles...",
    "...s'appelle Camille",
    "Bu?",
    "Blblblblbl",
    "Cami!",
    "Hehehehe...",
    "AMILLE!"
],
[
    "L'une d'elles...",
    "...s'appelle Caroline",
    "Ro?",
    "Pblfpfpblbl",
    "Caoline!",
    "Hehe!",
    "AROLINE!"
],
[
    "L'une d'elles...",
    "...s'appelle Laetitia",
    "Re!",
    "Agagaga!",
    "Lae..ti..ta...",
    "Hihihihihehe!",
    "ATATA!"
],
[
    "L'une d'elles...",
    "...s'appelle Anne",
    "Da!",
    "Plplt ",
    "Annnn!",
    "Ho? ",
    "NA!"
],
[
    "L'une d'elles...",
    "...est allergique aux écureuils",
    "C'est mon papa qui me l'a dit...",
    "Ça veut dire que je ressemble à un écureuil quand j'ai mes allergies. ",
    "Le drame de ma vie. ",
    "C'est parce que je suis allergique aux noisettes, et que les écureuils mangent des noisettes. ",
    "Du coup, on peut pas en adopter."
],
[
    "L'une d'elles...",
    "...confond sa droite et sa gauche",
    "Par contre haut et bas c'est bon, je gère.",
    "J'ai encore tout le temps d'apprendre.",
    "N'importe quoi ! J'ai juste une fois sur deux ! ",
    "C'est parce que je suis ambidextre.",
    "En fait la droite ça va, c'est la gauche qui me pose encore problème. "
],
[
    "L'une d'elles...",
    "...mange ses crottes de nez",
    "Seulement deux calories !",
    "Qu'est-ce que vous voulez que j'en fasse d'autre ?",
    "Pour info, ça a un goût de mini-bretzel.",
    "Ça et les croûtes. Je préfère les croûtes.",
    "C'est plein de microbes affaiblis, et qu'est-ce qui est aussi plein de microbes affaiblis ? Ouais : les vaccins !"
],
[
    "L'une d'elles...",
    "...dort encore avec son nounours",
    "Il s'appelle Monsieur Gluant...parce que je bave. ",
    "Ben quoi ? Tout le monde le fait, non ? ",
    "Ouaip. À prendre ou à laisser.",
    "Et alors?",
    "C'est pas un nounours, c'est un PANDA GEANT MANGEUR D'HOMMES ! "
],
[
    "L'une d'elles...",
    "...a peur en avion.",
    "Je connais quelqu'un qui connaît quelqu'un qui est mort dans un crash.",
    "C'est ce qui s'appelle être rationnelle.",
    "Tout ce qui monte doit redescendre. J'invente rien !",
    "J'ai pas peur en avion, j'ai peur de mourir dans un crash d'avion, nuance ! ",
    "Les hélicoptères par contre ça va."
],
[
    "L'une d'elles... ",
    "...joue dans un groupe. ",
    "En fait, on appelle ça un quatuor. Je suis au violoncelle. ",
    "On ne s'est pas encore mis d'accord sur le nom. Ça se joue entre Excroissance Absconse et Asphalte Fantôme.",
    "J'écris les chansons. La dernière était sur la guerre en Afghanistan. ",
    "Pour le moment on ne fait que des reprises de Green Day, mais on pense étendre à Good Charlotte très bientôt. ",
    "Ne me demandez pas le genre musical, je suis juste la batteuse. "
],
[
    "L'une d'elles...",
    "...est vegan",
    "Si c'est mignon j'en mange pas.",
    "Végétalienne en fait. Je fais pas ça pour la hype.",
    "L'industrie de la viande est plus polluante que les voitures !",
    "Et OUI, j'ai toutes les protéines dont j'ai besoin, merci ! ",
    "Je fais juste une exception pour le fromage, parce que le fromage, quoi..."
],
[
    "L'une d'elles...",
    "...a arrêté de fumer du jour au lendemain",
    "J'ai fait un trou sur mon cachemire, ça a été comme un électrochoc. ",
    "Pour être tout à fait franche, je n'avais commencé que deux semaines plus tôt.",
    "Je croyais avoir un cancer du poumon. En fait c'était une angine.",
    "Trois fois, même!",
    "Je veux pas me la raconter mais...c'était plutôt facile."
],
[
    "L'une d'elles...",
    "...n'a pas aimé Game of Thrones",
    "Le générique était quand même joli.",
    "J'ai préféré les bouquins.",
    "Donc si vous pouviez TOUS arrêter d'en parler s'il vous plaît ! ",
    "Des épées, des seins, des épées, des seins, des épées, des seins...c'est bon, on a pigé.",
    "Spoiler: l'épisode 1 est super chiant."
],
[
    "L'une d'elles...",
    "...cuisine de délicieux cheesecakes ",
    "Non, je ne donnerai pas la recette. Une femme a ses secrets. ",
    "Je ne sais pas s'ils sont délicieux, mais c'est sûr qu'on m'en redemande. ",
    "Et ils sont que pour moi! ",
    "Le plus dur c'est d'attendre qu'ils refroidissent.",
    "Mon ingrédient secret ? La faim. "
],
[
    "L'une d'elles...",
    "...a fait du roller derby",
    "Je n'étais peut-être pas la meilleure joueuse, mais j'étais la plus sexy ! Graou !",
    "Si j'ai pas fini en équipe de France, c'est qu'il n'y en avait pas à l'époque. ",
    "Ce qui me manque le plus, ce sont les chaussettes montantes.",
    "Le club m'a viré après trois ans : j'avais toujours rien compris aux règles. ",
    "On m'appelait la briseuse de rotules...jusqu'à ce que je brise les miennes. "
],
[
    "L'une d'elles...",
    "...arrive à lécher son coude",
    "Talent inutile n°32.",
    "Honnêtement, j'en ai un peu marre de faire des démonstrations. ",
    "Et la question que vous vous posez tous : est-ce que mon bras est très court ou bien ma langue très longue ?",
    "Mon genou aussi, pour ce que ça vaut.",
    "Je suis trop vieille pour ces conneries."
],
[
    "L'une d'elles...",
    "...est victime de violences conjugales."
]
];

var msg1, msg2, msg3, msg4, msg5, msg6, msg7;

var PSA = [
    "Dans l'Union européenne, une femme sur cinq a été victime\nde violence physique et/ou sexuelle dans le couple.",
    "Deux femmes sur cinq ont été victimes de violences\npsychologiques dans le couple.",
    "Ce n'est jamais acceptable, ni justifiable, ni tolérable.",
    " est seulement l'une d'elles.",
    "Source",
    "En savoir plus",
    "Rejouer"
];

var abusedName;

var loadingScreenTimer,
    loadingScreenTimerDelay = 200,
    updateLoadingScreen = (function() {
        var text = 'Chargement',
            dots = 0,
            container = document.getElementById('p5_loading-text');

        return function() {
            var _text = text;
            for (var i = 0; i < dots; ++i) { _text += '.'; }
            container.textContent = _text;
            loadingScreenTimer = window.setTimeout(updateLoadingScreen, loadingScreenTimerDelay);
            if (++dots > 3) { dots = 0; }
        };
    })(),
    cancelLoadingScreen = function() {
        window.clearTimeout(loadingScreenTimer);
    };

function preload() {
  updateLoadingScreen();

  //load the animations
  animA = loadAnimation("assets/sprites/A0.png", "assets/sprites/A16.png");
  animB = loadAnimation("assets/sprites/B0.png", "assets/sprites/B16.png");
  animC = loadAnimation("assets/sprites/C0.png", "assets/sprites/C16.png");
  animD = loadAnimation("assets/sprites/D0.png", "assets/sprites/D16.png");
  animE = loadAnimation("assets/sprites/E0.png", "assets/sprites/E16.png");

  bg = loadImage("assets/800bg.png");
  title = loadImage("assets/title.png");

  fontBold = loadFont("assets/PerfectDOS.ttf");
  fontNormal = loadFont("assets/Volter__28Goldfish_29.ttf");

  //music = loadSound(['assets/loop.ogg','assets/loop.mp3']);

  //music = loadSound('assets/music.mp3');
  music = loadSound("assets/loop.mp3");

  hit = loadSound('assets/hit.mp3');


}

function setup() {
  createCanvas(800,600);
  noSmooth();

  //create character
  A = createSprite(100, POS_Y);
  //assign animation "life" each frame is a stage
  A.addAnimation("life", animA);
  //create a property id to access the text in the array
  A.id = 0;
  //assign the same function to every characters to detect click
  A.onMouseReleased = respond;
  //subtle movement when selected
  A.onMouseOver = function() { if(gameState=="game") this.position.y = POS_Y-2; }
  A.onMouseOut = function() { this.position.y = POS_Y;}

  B = createSprite(250, POS_Y);
  B.addAnimation("life", animB);
  B.id = 1;
  B.onMouseReleased = respond;
  B.onMouseOver = function() { if(gameState=="game") this.position.y = POS_Y-2; }
  B.onMouseOut = function() { this.position.y = POS_Y;}

  C = createSprite(400, POS_Y);
  C.addAnimation("life", animC);
  C.id = 2;
  C.onMouseReleased = respond;
  C.onMouseOver = function() { if(gameState=="game") this.position.y = POS_Y-2; }
  C.onMouseOut = function() { this.position.y = POS_Y;}

  D = createSprite(550, POS_Y);
  D.addAnimation("life", animD);
  D.id = 3;
  D.onMouseReleased = respond;
  D.onMouseOver = function() { if(gameState=="game") this.position.y = POS_Y-2; }
  D.onMouseOut = function() { this.position.y = POS_Y;}

  E = createSprite(700, POS_Y);
  E.addAnimation("life", animE);
  E.id = 4;
  E.onMouseReleased = respond;
  E.onMouseOver = function() { if(gameState=="game") this.position.y = POS_Y-2; }
  E.onMouseOut = function() { this.position.y = POS_Y;}

  //create the general textfields
  oneOfThem = createTextField("L'une d'elles...", 20, 30, 780, 200);
  oneOfThem.font = fontBold;
  oneOfThem.size = 32;

  //temporary text
  prompt = createTextField("", 20, 80, 780, 200);
  prompt.font = fontBold;
  prompt.size = 32;

  //instructions
  instructions = createTextField("(cliquez sur un personnage)", 380, 93, 500, 200);
  instructions.font = fontBold;
  instructions.size = 16;
  instructions.visible = false;

  //create character text response as property of the sprite
  A.response = createTextField("...", A.position.x, TEXT_Y,TEXT_W,TEXT_H);
  A.response.font = fontNormal;
  A.response.size = 18;
  A.response.textStroke = 4;
  A.response.textFill = color(255, 130, 255); //pink

  B.response = createTextField("...", B.position.x, TEXT_Y,TEXT_W,TEXT_H);
  B.response.font = fontNormal;
  B.response.size = 18;
  B.response.textStroke = 4;
  B.response.textFill = color(132, 255, 132); //green

  C.response = createTextField("...", C.position.x, TEXT_Y,TEXT_W,TEXT_H);
  C.response.font = fontNormal;
  C.response.size = 18;
  C.response.textStroke = 4;
  C.response.textFill = color(132, 255, 255); //cyan

  D.response = createTextField("...", D.position.x, TEXT_Y,TEXT_W,TEXT_H);
  D.response.font = fontNormal;
  D.response.size = 18;
  D.response.textStroke = 4;
  D.response.textFill = color(198, 195, 255); //violet

  E.response = createTextField("...", E.position.x, TEXT_Y, TEXT_W,TEXT_H);
  E.response.font = fontNormal;
  E.response.alignment = RIGHT;
  E.response.size = 18;
  E.response.textStroke = 4;
  E.response.textFill = color(255, 130, 132); //violet



  xButton = createSprite(width-20, 20);
  xButton.addImage(loadImage("assets/x.png"));
  xButton.onMouseReleased = function () {
	if(musicIsPlaying) { music.stop(); musicIsPlaying = false; xButton.alpha = 0.5;} else { music.loop(); musicIsPlaying = true; xButton.alpha = 1;}; };

  cancelLoadingScreen();
  newGame();
}


//start/reset states
function newGame() {

  xButton.visible = true;
  xButton.alpha = 1;
  musicIsPlaying = true;

  //set frame to zero
  A.animation.changeFrame(0);
  A.animation.stop();
  A.alpha = 1;
  A.selected = false;

  B.animation.changeFrame(0);
  B.animation.stop();
  B.alpha = 1;
  B.selected = false;

  C.animation.changeFrame(0);
  C.animation.stop();
  C.alpha = 1;
  C.selected = false;

  D.animation.changeFrame(0);
  D.animation.stop();
  D.alpha = 1;
  D.selected = false;

  E.animation.changeFrame(0);
  E.animation.stop();
  E.alpha = 1;
  E.selected = false;

  //make all the responses invisible
  A.response.visible = false;
  B.response.visible = false;
  C.response.visible = false;
  D.response.visible = false;
  E.response.visible = false;

  A.abused = B.abused = C.abused = D.abused = E.abused = false;

  //remove messages if they exist
  if(msg1!=undefined) msg1.remove();
  if(msg2!=undefined) msg2.remove();
  if(msg3!=undefined) msg3.remove();
  if(msg4!=undefined) msg4.remove();
  if(msg5!=undefined) msg5.remove();
  if(msg6!=undefined) msg6.remove();
  if(msg7!=undefined) msg7.remove();


  age = 0;//13+4;
  phase = 0;
  phaseCounter = 0;
  gameState = "title";


  music.stop();
}


function draw() {

  if(gameState == "title")
  {
  //draw title
  image(title);
  }
  if(gameState == "game")
  {
  //refresh background
  image(bg);

  //draw characters
  drawSprites();

  //one of them... (timed)
  if(phase==0)
  {

  //change text, make it visible
  //oneOfThem.txt = words[phase][0];
  oneOfThem.visible = true;

  //keep track of the time with a variable
  //phaseCounter++;
  var now = Date.now();
  timeSinceLastFrame = now - lastTime;
  lastTime = now;
  phaseCounter = phaseCounter + (timeSinceLastFrame/60);

  //if counter is over next phased
  if(phaseCounter >= ONE_DELAY)
    {
    phase++;
    phaseCounter = 0;
    }
  else
    {
    //find the alpha value from 0 to 255
    var a = map(phaseCounter, 0, FADE_DELAY, 255, 0);
    //change color, black with transparency
    fill(0,0,0,a);
    //rectangle
    rect(0,0,width, height);
    }

  //exception: if selected they disappear
  if(age<=4)
    {
    A.visible = !A.selected;
    B.visible = !B.selected;
    C.visible = !C.selected;
    D.visible = !D.selected;
    E.visible = !E.selected;
    }
  else
    A.visible = B.visible = C.visible = D.visible = E.visible = true;

  }

  //...prompt (wait for input)
  if(phase==1)
  {
  prompt.text = words[age][1];
  prompt.visible = true;

  //instruction only the fist time
  if(age==0)
    {
    //phaseCounter++;
  var now = Date.now();
  timeSinceLastFrame = now - lastTime;
  lastTime = now;
  phaseCounter = phaseCounter + (timeSinceLastFrame/60);

	if(phaseCounter >= ONE_DELAY)
      instructions.visible = true;
    }
  }

  //response (wait for click)
  if(phase==2)
  {
  //nothing here see mouseReleased

  }

  //fade
  if(phase==3)
  {
  //phaseCounter++;
    var now = Date.now();
  timeSinceLastFrame = now - lastTime;
  lastTime = now;
  phaseCounter = phaseCounter + (timeSinceLastFrame/60);

  //find the alpha value from 0 to 255
  var a = map(phaseCounter, 0, FADE_DELAY, 0, 255);
  //change color, black with transparency
  fill(0,0,0,a);
  //rectangle
  rect(0,0,width, height);

  //end of fade go to next age
  if(phaseCounter>FADE_DELAY)
    {
    phase = 0;
    phaseCounter = 0;
    age++;
    //make all the responses invisible
    A.response.visible = false;
    B.response.visible = false;
    C.response.visible = false;
    D.response.visible = false;
    E.response.visible = false;

    //make them grow
    if(age>4)
      {
      A.animation.changeFrame(age-4);
      B.animation.changeFrame(age-4);
      C.animation.changeFrame(age-4);
      D.animation.changeFrame(age-4);
      E.animation.changeFrame(age-4);
      }
    }
  }
  }//end gameState "game ///////////////////////
  else if(gameState == "ending")
    {
    //refresh background
    image(bg);

    //draw characters
    drawSprites();

    //phaseCounter++;
  var now = Date.now();
  timeSinceLastFrame = now - lastTime;
  lastTime = now;
  phaseCounter = phaseCounter + (timeSinceLastFrame/60);

    if(phase == 0)
      {
      oneOfThem.visible = false;
      prompt.visible = false;

      if(phaseCounter>MSG_DELAY/2)
        {
        phase=1;
        phaseCounter = 0;
        }
      }

       //characters fade
      if(phase == 1)
      {
      var a = map(phaseCounter, 0, 150, 1, 0);

        //fade the others
        if(!A.abused) A.alpha = a;
        if(!B.abused) B.alpha = a;
        if(!C.abused) C.alpha = a;
        if(!D.abused) D.alpha = a;
        if(!E.abused) E.alpha = a;


      if(phaseCounter>150)
        {
        msg1  = createTextField(PSA[0], 25, 34,650,100);
        msg1.font = fontBold;
        msg1.size = 22;
        msg1.textStroke = 0;
        msg1.textFill = color(00, 00, 00);
        msg1.alpha = 0;

        phase=2;
        phaseCounter = 0;
        }
      }

      if(phase == 2)
      {
      msg1.alpha = map(phaseCounter, 0, MSG_DELAY/2, 0, 1);

      if(phaseCounter>MSG_DELAY)
        {
        msg2  = createTextField(PSA[1], 25, 102,700,100);
        msg2.font = fontBold;
        msg2.size = 22;
        msg2.textStroke = 0;
        msg2.textFill = color(0, 0, 0);
        msg2.alpha = 0;

        phase=3;
        phaseCounter = 0;
        }
      }

      if(phase == 3)
      {
      msg2.alpha = map(phaseCounter, 0, MSG_DELAY/2, 0, 1);

      if(phaseCounter>MSG_DELAY)
        {
        msg3  = createTextField(PSA[2], 25, 170,700,100);
        msg3.font = fontBold;
        msg3.size = 22;
        msg3.textStroke = 0;
        msg3.textFill = color(0, 0, 0);
        msg3.alpha = 0;

        phase=4;
        phaseCounter = 0;
        }
      }

    if(phase == 4)
      {
      msg3.alpha = map(phaseCounter, 0, MSG_DELAY/2, 0, 1);

      if(phaseCounter>MSG_DELAY)
        {
        msg4  = createTextField(abusedName+PSA[3], 25, 202,700,100);
        msg4.font = fontBold;
        msg4.size = 22;
        msg4.textStroke = 0;
        msg4.textFill = color(0, 0, 0);
        msg4.alpha = 0;
        phase=5;
        phaseCounter = 0;
        }
      }

      if(phase == 5)
      {
      msg4.alpha = map(phaseCounter, 0, MSG_DELAY/2, 0, 1);

      if(phaseCounter>MSG_DELAY)
        {
		//source
        msg5  = createTextField(PSA[4], 660, 570, 100,20);
        msg5.font = fontBold;
        msg5.size = 20;
        msg5.textStroke = 0;
        msg5.textFill = color(0, 0, 0);
        msg5.alpha = 0;
        msg5.onMouseOver = function() {this.textFill = color(255, 255, 255); }
        msg5.onMouseOut = function() {this.textFill = color(0, 0, 0); }
        msg5.onMouseReleased = function() {window.open("http://fra.europa.eu/en/publication/2014/violence-against-women-eu-wide-survey-main-results-report",'_blank'); }
        //learn more
        msg6  = createTextField(PSA[5], 335, 570, 120, 20);
        msg6.font = fontBold;
        msg6.size = 00;
        msg6.textStroke = 0;
        msg6.textFill = color(0, 0, 0);
        msg6.alpha = 0;
        msg6.onMouseOver = function() {this.textFill = color(255, 255, 255); }
        msg6.onMouseOut = function() {this.textFill = color(0, 0, 0); }
        msg6.onMouseReleased = function() {window.open("http://www.dvrcv.org.au/help-advice",'_blank'); }
		//replay
		msg7  = createTextField(PSA[6], 50, 570, 100, 20);
        msg7.font = fontBold;
        msg7.size = 20;
        msg7.textStroke = 0;
        msg7.textFill = color(0, 0, 0);
        msg7.alpha = 0;
        msg7.onMouseOver = function() {this.textFill = color(255, 255, 255); }
        msg7.onMouseOut = function() {this.textFill = color(0, 0, 0); }
        msg7.onMouseReleased = newGame;


        phase=6;
        phaseCounter = 0;
        }
      }

      //end state
      if(phase == 6)
      {
      msg5.alpha = map(phaseCounter, 0, MSG_DELAY/2, 0, 1);
      msg6.alpha = map(phaseCounter, 0, MSG_DELAY/2, 0, 1);
	  msg7.alpha = map(phaseCounter, 0, MSG_DELAY/2, 0, 1);
      }

    }
}


//called when clicked on a character, this is the reference to the character
function respond() {

  //respond only if it's the right phase
  if(phase==1 && this.visible)
    {
    //conclusion
    if(age==13+4)
      {
      if(gameState=="game")
        {
        //play sound
		xButton.visible = false;
        music.stop();
        hit.play();

        this.animation.changeFrame(16);
        this.position.y = POS_Y;
        this.abused = true;
        abusedName = this.name;
        gameState = "ending";
        phase = 0;
        phaseCounter = 0;
        }
      }
    else
      {
      oneOfThem.visible = false;
      prompt.visible = false;
      instructions.visible = false;
      print(round(TEXT_W/18));
      //2 is offsetting the first general prompts
      this.response.text = words[age][2+this.id];
      this.response.addBreaks(21);
      this.response.visible = true;

      //save a variable to make them disappear in the naming phase
      this.selected = true;

      //save the name
      if(age==0)
        this.name = "Sandra";
      if(age==1)
        this.name = "Camille";
      if(age==2)
        this.name = "Caroline";
      if(age==3)
        this.name = "Laetitia";
      if(age==4)
        this.name = "Anne";

      phase = 2;
      }

    }



}

//p5 function called automatically every click
function mouseReleased()
{
  //start game
  if(gameState == "title")
    {
    gameState = "game";
    music.playMode("restart");
    music.loop();
    }

  //respond only if it's the right phase
  if(phase==2 && gameState !== 'ending')
    {
    //fade to black
    phase = 3;
    phaseCounter = 0;
    }
}




function createTextField(txt, px, py, w, h)
{
  var tBox = createSprite(px, py, w, h);
  tBox.arr;

  tBox.position.x = px;
  tBox.position.y = py;
  tBox.width = w;
  tBox.height = h;
  tBox.textStroke = 0;
  tBox.character = 0;
  tBox.speed = -1;
  tBox.cycles = 0;
  tBox.boxFill = color(0,0,0,0);
  tBox.textFill = color(0,0,0);
  tBox.strokeColor = color(0,0,0);
  tBox.size = 20;
  tBox.style = NORMAL;
  tBox.font = 'sans-serif';

  tBox.margin = 0;
  tBox.marginColor = color(0,0,0,0);

  tBox.paddingLeft = 0;
  tBox.paddingRight = 0;
  tBox.paddingTop = 0;
  tBox.paddingBottom = 0;
  tBox.longestLine = 0;
  tBox.alignment = LEFT;
  tBox.backgroundImage;

  tBox.text = txt;

  //text is from the left so
  tBox.collider = new AABB(createVector(tBox.position.x+tBox.width/2, tBox.position.y+tBox.height/2), createVector(tBox.width, tBox.height));


  //for finnicky fonts do wrapping manually
  tBox.addBreaks = function(s) {

  this.arr = [];
  var lastLine = 0;
  var lastWhiteSpace = 0;
  var chars = 0;
  this.longestLine = 0;

  //s is the max characters per line
  for(var i = 0; i<this.text.length; i++)
    {
    //print(this.text.charAt(i)+ " white "+lastWhiteSpace);

    chars++;

    if(this.text.charAt(i)==" ")
    {
      lastWhiteSpace = i;
    }

    if(chars>s)
      {
      var line = this.text.substring(lastLine, lastWhiteSpace+1);

      //save this for alignment
      if(line.length>this.longestLine)
        this.longestLine = line.length;

      this.arr.push(line);
      lastLine = lastWhiteSpace+1;
      //this.text = this.text.insert(lastWhiteSpace+1, "\n");
      chars = 0;
      }
    }

    line = this.text.substring(lastLine, this.text.length);

    //save this for alignment
    if(line.length>this.longestLine)
      this.longestLine = line.length;

    this.arr.push(line);


  }

  //override
  tBox.draw = function() {
  this.cycles++;

  if(this.speed==-1)
  {
  this.character = this.text.length;
  }
  else if(this.cycles >= this.speed)
  {
    this.cycles = 0;
    this.character++;
    if(this.character>=this.text.length)
      this.character = this.text.length;
  }

  var displayedText = this.text.substring(0, this.character);

  push();
  rectMode(CORNER);
  textAlign(LEFT, TOP);

  textFont(this.font);
  textSize(this.size);
  textStyle(this.style);


  //box
  fill(this.boxFill);
  strokeWeight(this.margin);
  stroke(this.marginColor);
  rect(0,0, this.width+this.paddingLeft+this.paddingRight, this.height+this.paddingTop+this.paddingBottom);

  if(this.alpha==1)
    fill(this.textFill);
  else
    fill(color(red(this.textFill), green(this.textFill), blue(this.textFill), map(this.alpha,0,1,0,255)));


  if(this.textStroke == 0)
    noStroke();
  else
  {
  drawingContext.lineWidth = this.textStroke;
  if(this.alpha==1)
    drawingContext.strokeStyle = this.strokeColor;
  else
    {
    if(this.alpha<0) this.alpha = 0;
    if(this.alpha>1) this.alpha = 1;

    drawingContext.strokeStyle = color(red(this.strokeColor), green(this.strokeColor), blue(this.strokeColor), map(this.alpha,0,1,0,255));
    }
  }



  //make it not blurry
  if(this.arr==undefined)
  text(displayedText, this.paddingLeft,this.paddingTop,this.width-this.paddingRight-this.paddingLeft, this.height-this.paddingBottom-this.paddingTop);
  else
  {
  for(var i=0; i<this.arr.length; i++)
    {
    var posX = round(this.paddingLeft-this.longestLine/2*9)

    if(this.position.x +this.longestLine*9/2 +20 > width)
    {
    posX -= this.longestLine*9/2-20;
    }

    if(this.position.x -this.longestLine*9/2 -30 < 0)
    {
    posX += 30;
    }

    text(this.arr[i], round(posX),round(this.paddingTop+i*(this.size+8)),this.width-this.paddingRight-this.paddingLeft, this.height-this.paddingBottom-this.paddingTop);
    }

  }

  pop();

  }

  return tBox;
  //tBox
}
